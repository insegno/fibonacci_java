import java.util.Scanner;

/**
 *
 */
public class Fibonacci {

    /**
     *
     * @param argc arguments
     */
    public static void main(String[] argc) {

        // ask user for how many number to print
        int n = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Numeri della sequenza da stampare: ");
        n = scanner.nextInt();

        // initialize first and second number of the sequence
        int a = 0;
        int b = 1;

        // declare next number in the sequence
        int c;

        // print first two numbers
        System.out.print(a + " ");
        System.out.print(b + " ");

        // cycle n times
        for (int i = 0; i < 20; i++) {
            c = a + b;
            a = b;
            b = c;

            System.out.print(c + " ");
        }

    }

}
